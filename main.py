# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
from jupyter_dash import JupyterDash
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import dash_bootstrap_components as dbc
import os
import pandas as pd
import plotly.graph_objects as go
from functions import stock_pull_func as stock_pull_func
from functions import stock_plot_func as stock_plot_func
from functions import page_func as page_func


# %%
app = JupyterDash(
    __name__, 
    external_stylesheets=[dbc.themes.BOOTSTRAP,os.path.join("static","css","mystyle1.css")],
    meta_tags=[{'name': 'viewport', 'content': 'width=device-width, initial-scale=1'}]
    )
server = app.server


# %%
APP_LOGO = os.path.join("static","logo_white.png")
#CODE_PATH = r"https://gitlab.com/choycheehoe/project_finance.git"
CODE_PATH = r"https://gitlab.com/"


# %%
tab1_content = dbc.Card(
    dbc.CardBody(
        [
            page_func.create_stock_search_bar(),
            html.Div(id='tab1_header'),
            html.Div(id='tab1_stock_price_plot'),
            html.Div(id='intermediate_chosen_stock', style={'display': 'none'})
        ]
    ),
)

tab2_content = dbc.Card(
    dbc.CardBody(
        [
            html.Div(id='tab2_header'),
            html.Div(id='tab2_stock_price_plot'),   
            html.P("=)", className="card-text"),
        ]
    ),
)

tab3_content = dbc.Card(
    dbc.CardBody(
        [
            html.Div(id='tab3_header',style={'padding-bottom':'5px'}),
            
            html.A('Jump to'),
            html.A('Balance Sheet',href='#tab3_stock_balance_sheet_plot',style={'padding-left':'5px'}),
            html.A('|',style={'padding-left':'5px'}),
            html.A('Income Statement',href='#tab3_stock_income_statement_plot',style={'padding-left':'5px'}),
            html.A('|',style={'padding-left':'5px'}),
            html.A('Cash Flow',href='#tab3_stock_cash_flow_plot',style={'padding-left':'5px'}),
            
            html.Div(id='tab3_stock_balance_sheet_plot'),

            html.A('Jump to'),
            html.A('Balance Sheet',href='#tab3_stock_balance_sheet_plot',style={'padding-left':'5px'}),
            html.A('|',style={'padding-left':'5px'}),
            html.A('Income Statement',href='#tab3_stock_income_statement_plot',style={'padding-left':'5px'}),
            html.A('|',style={'padding-left':'5px'}),
            html.A('Cash Flow',href='#tab3_stock_cash_flow_plot',style={'padding-left':'5px'}),
                        
            html.Div(id='tab3_stock_income_statement_plot'),

            html.A('Jump to'),
            html.A('Balance Sheet',href='#tab3_stock_balance_sheet_plot',style={'padding-left':'5px'}),
            html.A('|',style={'padding-left':'5px'}),
            html.A('Income Statement',href='#tab3_stock_income_statement_plot',style={'padding-left':'5px'}),
            html.A('|',style={'padding-left':'5px'}),
            html.A('Cash Flow',href='#tab3_stock_cash_flow_plot',style={'padding-left':'5px'}),
                                    
            html.Div(id='tab3_stock_cash_flow_plot'),

            html.A('Jump to'),
            html.A('Balance Sheet',href='#tab3_stock_balance_sheet_plot',style={'padding-left':'5px'}),
            html.A('|',style={'padding-left':'5px'}),
            html.A('Income Statement',href='#tab3_stock_income_statement_plot',style={'padding-left':'5px'}),
            html.A('|',style={'padding-left':'5px'}),
            html.A('Cash Flow',href='#tab3_stock_cash_flow_plot',style={'padding-left':'5px'}),

        ]
    ),
)

tab4_content = dbc.Card(
    dbc.CardBody(
        [
            page_func.create_compare_stock_box(),
            html.Div(id='tab4_compare_stock_list'),
            html.Div(id='tab4_comparison_plot'),
            html.P("=)", className="card-text"),
            html.Div(children='', id='intermediate_compare_stock_list', style={'display': 'none'}),
            html.Div(children='0',id='intermediate_compare_stock_del_nclick', style={'display': 'none'}),
            html.Div(children='0',id='intermediate_compare_stock_add_nclick', style={'display': 'none'}),
            html.Div(children='0',id='intermediate_compare_stock_clear_nclick', style={'display': 'none'}),
        ]
    ),
)

tabs = dbc.Tabs(
    [
        dbc.Tab(tab1_content, label="Overview", id="tab1", disabled=False),
        dbc.Tab(tab2_content, label="Price Performance", id="tab2", disabled=True),
        dbc.Tab(tab3_content, label="Financial",id="tab3", disabled=True),
        dbc.Tab(tab4_content, label="Comparison",id="tab4", disabled=False),
    ]
    ,
    id='tab_table'
)


# %%
app.layout = html.Div([
    #dbc.Container(page_func.create_row_ruler(),fluid=True),
    page_func.create_navbar(APP_LOGO=APP_LOGO,CODE_PATH=CODE_PATH),
    tabs])

@app.callback(
    [Output("stock_code_loading_logo_output", "children"),
    Output('stock_code_input_text', 'valid'),Output('stock_code_input_text', 'invalid'),
    Output('tab1_header',"children"),
    Output('tab1_stock_price_plot', 'children'),
    Output('tab2_header',"children"),
    Output('tab2_stock_price_plot', 'children'),
    Output('tab2', 'disabled'),
    Output('tab3_header',"children"),
    Output('tab3_stock_balance_sheet_plot', 'children'),
    Output('tab3_stock_income_statement_plot', 'children'),
    Output('tab3_stock_cash_flow_plot', 'children'),
    Output('tab3', 'disabled'),
    Output('tab4', 'disabled'),
    Output('intermediate_chosen_stock', 'children'),
    ], 
    [Input("stock_code_search_button", "n_clicks"),State('stock_code_input_text', 'value')]
)
def on_stock_code_button_click(n,value):
    output_message_0 = " "
    
    figlist = dict()
    figlist['stock_price'] = []
    figlist['balance_sheet'] = []
    figlist['income_statement'] = []
    figlist['cash_flow'] = []
    stock_fullname =""
    stock_symbol = ""

    # change input appearance by stocking if the stockcode is valid
    if (value is None) or (len(value)<=0):
        flag_1 = False
        flag_2 = False
        tab2_flag = True
        tab3_flag = True
        tab4_flag = True
        output_message_0 = "Please insert a stock code above. (e.g. D05.SI)"
        stock_symbol = ""

    elif stock_pull_func.check_for_valid_code(value):
        stock_symbol = value

        # fetch stock data from yahoo finance
        stock_info = stock_pull_func.read_in_yahoo_data(stock_symbol)

        # get company name
        stock_fullname = stock_info["stock_symbol"]
        if "longName" in stock_info.keys():
            stock_fullname = '{} ({})'.format(stock_info['longName'], stock_fullname)
        
        # plot price chart
        figlist['stock_price'] = [dcc.Graph(figure=page_func.create_stock_price_plot(stock_info))]

        # plot balance sheet etc tables
        for table_type in ['balance_sheet','income_statement','cash_flow']:    
            df = stock_info[table_type].T.copy()
            df = df.reset_index().rename(columns={'index':'variable'})
            df['variable'] = df['variable'].apply(lambda x: x.replace("'",""))
            df = df.astype(str)
            figlist[table_type] = [dcc.Graph(figure=stock_plot_func.plot_table(df, table_type.replace('_',' ').title(),height=30).update_layout(height=(((df.shape[0]+8)*30))))]
        # set output
        output_message_0 = "Successfully loaded {}.".format(stock_symbol)
        flag_1 = True
        flag_2 = False
        tab2_flag = False
        tab3_flag = False
        tab4_flag = False
        stock_symbol
    else:
        flag_1 = False
        flag_2 = True
        tab2_flag = True
        tab3_flag = True
        tab4_flag = True
        output_message_0 = "Invalid stock code. Please try again."
        stock_symbol = ""

    return (output_message_0,flag_1,flag_2,
    stock_fullname,figlist['stock_price'],
    stock_fullname,figlist['stock_price'],tab2_flag,
    stock_fullname,figlist['balance_sheet'],figlist['income_statement'],figlist['cash_flow'],tab3_flag,
    tab4_flag,
    stock_symbol)

@app.callback(
    [
        Output("tab4_compare_stock_list", "children"),
        Output("intermediate_compare_stock_list", "children"),
        Output("intermediate_compare_stock_add_nclick", "children"),
        Output("intermediate_compare_stock_del_nclick", "children"),
        Output("intermediate_compare_stock_clear_nclick", "children"),
        Output('compare_stock_input_text', 'valid'), Output('compare_stock_input_text', 'invalid'),

    ],
    [
        Input("compare_stock_add_button", "n_clicks"),
        Input("compare_stock_del_button", "n_clicks"),
        Input("compare_stock_clear_button", "n_clicks"),
        State("intermediate_compare_stock_add_nclick", "children"),
        State("intermediate_compare_stock_del_nclick", "children"),
        State("intermediate_compare_stock_clear_nclick", "children"),
        State("intermediate_compare_stock_list", "children"),
        State('compare_stock_input_text', 'value')
        
    ]
)
def compare_stock_add_button_click(add_nclick, del_nclick, clear_nclick, stored_add_nclick, stored_del_nclick, stored_clear_nclick, stored_stock_list, value1):
    print(stored_stock_list)
    if type(stored_stock_list) != list:
        stored_stock_list = list()
    if type(value1) != str:
        value1 = ''        
    if stored_del_nclick is None:
        stored_del_nclick = 0
    if stored_add_nclick is None:
        stored_add_nclick = 0
    if stored_clear_nclick is None:
        stored_clear_nclick = 0
    if del_nclick is None:
        del_nclick = 0
    if add_nclick is None:
        add_nclick = 0
    if clear_nclick is None:
        clear_nclick = 0
        
    if len(value1) < 0:
        flag_1 = False
        flag_2 = False
    else:
        if stock_pull_func.check_for_valid_code(value1):
            value1 = value1.strip()
            if del_nclick>int(stored_del_nclick):
                if value1 in stored_stock_list:
                    stored_stock_list.remove(value1)
            elif clear_nclick>int(stored_clear_nclick):
                stored_stock_list = []
            else:
                if value1 not in stored_stock_list:
                    stored_stock_list.append(value1)                

            flag_1 = True
            flag_2 = False
        else:
            flag_1 = False
            flag_2 = True
    return ["Comparing with stocks: {}".format(stored_stock_list),stored_stock_list, add_nclick, del_nclick, clear_nclick, flag_1, flag_2]

@app.callback(
    [
        Output("compare_stock_loading_logo_output", "children"),
        Output('tab4_comparison_plot', 'children'),
    ],
    [
        Input("compare_stock_start_button", "n_clicks"),
        State("intermediate_compare_stock_list", "children"),
        State('intermediate_chosen_stock', 'children'),
        
    ]
)
def compare_stock_start_button_click(start_nclick,stock_list,chosen_stock):
    stock_list = [chosen_stock]+stock_list
    stock_list = [i.replace("'","").replace('"','') for i in stock_list]
    stock_list = [i for i in stock_list if len(i) > 0]
    
    if len(stock_list) > 1:
        df = page_func.compare_stock_statistics_func(stock_list)
        df = df.reset_index().rename(columns={'index':'variable'})
        fig = [dcc.Graph(figure=stock_plot_func.plot_table(df, "Comparison".title()).update_layout(height=(((df.shape[0]+10)*30))))]
    else:
        fig = ""
    return ["", fig]


# %%
app.run_server(mode="inline")


# %%



# %%



# %%



# %%



# %%



# %%



# %%



# %%




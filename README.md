# Objectives of this project
- to learn how to 
    - pull data from website
    - build dashboard using dash
        - to apply css, html, plotly and dash
    - to analyse stock data (pending work)
- to study a stock to see if it is worth investing in.  

# Main packages used
- data are pulled mainly from yahoo finance using
    - yfinance
    - BeautifulSoup
- plots are generated using
    - plotly
- Website is built using
    - dash
    - jupyter_dash (used for development on jupyter notebook)

# Demo
- Here's a sample video:

![Sample Video](img/demo.mp4)

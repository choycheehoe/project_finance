import sys
import os 
import sys
sys.path.append(os.path.join(os.path.dirname(__file__)))
from jupyter_dash import JupyterDash
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import dash_bootstrap_components as dbc
import pandas as pd
import plotly.graph_objects as go
import stock_pull_func as stock_pull_func
import stock_plot_func as stock_plot_func

# functions

def create_stock_price_plot(stock_info):
    #stock_fullname = stock_info["stock_symbol"]
    #if "longName" in stock_info.keys():
    #    stock_fullname = '{} ({}) Price Chart'.format(stock_info['longName'], stock_fullname)
    #print(stock_fullname)
    fig = stock_plot_func.plot_stock_price(stock_info['history'], "Price Chart")
    return fig


def compare_stock_statistics_func(stock_symbol_list):
    stock_statistics = pd.DataFrame()
    for current_symbol in stock_symbol_list:
        print(current_symbol)
        current_stock_info = stock_pull_func.read_in_yahoo_data(current_symbol,period='7d')
        Current_price = current_stock_info['history'].copy().iloc[-1,:]
        if ('longName' in list(current_stock_info.keys())):
            if (pd.notnull(current_stock_info['longName'])):
                current_name = '{} ({})'.format(current_stock_info['longName'], current_symbol)
        else:
            current_name = current_symbol
        df = current_stock_info['key_statistics']
        df.loc['Date','value'] = pd.to_datetime(Current_price['date']).strftime("%d/%m/%Y")
        df.loc['Current price','value'] = Current_price['close']
        df = df.rename(columns={'value':current_name})
        stock_statistics = pd.concat([stock_statistics,df],axis=1)

    key_stat_list = ['Date', 'Current price', '52-week high', '52-week low', 'Trailing annual dividend yield', 'Trailing P/E', 'Price/sales (ttm)', 'Price/book (mrq)', 'Profit margin', 'Operating margin (ttm)', 'Return on assets (ttm)', 'Return on equity (ttm)', 'Revenue (ttm)', 'Gross profit (ttm)', 'Total cash (mrq)', 'Total cash per share (mrq)', 'Total debt (mrq)', 'Total debt/equity (mrq)', 'Current ratio (mrq)', 'Operating cash flow (ttm)', 'Levered free cash flow (ttm)']

    return stock_statistics.loc[key_stat_list,:]


# creating navbar
def create_navbar(APP_LOGO,CODE_PATH):
    # create logo symbol
    logo_obj = html.A([
            html.Img(src=APP_LOGO,style={'height': '8%','width': '8%',"vertical-align": "middle"}),
            html.A('Monkey Finance',style={'font-size':"2em",'padding': '8px',"vertical-align": "middle", "color": "#fafafa"})
    ], style={'text-align': 'left',"justify-content": "center"}, id="logo-obj")
    
    # create the code button
    code_button_obj = html.A(
            dbc.Button(r"Code", outline=True, color="light",style={'font-size':"1em"}, id="code-button"),
            href=CODE_PATH,
            className="ml-auto"
        )

    # form the navbar arrangement
    navbar = dbc.Navbar([
        logo_obj,
        code_button_obj
        ],
        color="primary",
        dark=True,
        id = "nav_header")

    return navbar

# creating stock search box
def create_stock_search_bar():
    search_bar = dbc.Row(
    [
        dbc.Col(dbc.Input(type="text", placeholder="Stock Code",id="stock_code_input_text", valid=False,invalid=False), width="auto"),
        dbc.Col(dbc.Button("Search", color="success", id="stock_code_search_button"),width=1,style={"padding-left": "5px"}),
    ],
    no_gutters=True,
    className="ml-auto flex-nowrap",
    align="center",
    style = {"padding-bottom": "10px"}
)

    search_loading_logo =  dcc.Loading(id="stock_code_loading_logo",
        type="dot",
        #type="circle",
        children=html.A("Please insert a stock code above. (e.g. D05.SI)", id="stock_code_loading_logo_output"))

    search_box = html.A(
        [search_bar,search_loading_logo],
        style = {"padding-bottom": "15px"}
    )

    return search_box

def create_row_ruler():
    row_ruler = dbc.Row(
            [
                dbc.Col(html.Div("1"),style={"background-color": "coral", "color": "white","text-align": "center"}),
                dbc.Col(html.Div("2"),style={"background-color": "black", "color": "white","text-align": "center"}),
                dbc.Col(html.Div("3"),style={"background-color": "coral", "color": "white","text-align": "center"}),
                dbc.Col(html.Div("4"),style={"background-color": "black", "color": "white","text-align": "center"}),
                dbc.Col(html.Div("5"),style={"background-color": "coral", "color": "white","text-align": "center"}),
                dbc.Col(html.Div("6"),style={"background-color": "black", "color": "white","text-align": "center"}),
                dbc.Col(html.Div("7"),style={"background-color": "coral", "color": "white","text-align": "center"}),
                dbc.Col(html.Div("8"),style={"background-color": "black", "color": "white","text-align": "center"}),
                dbc.Col(html.Div("9"),style={"background-color": "coral", "color": "white","text-align": "center"}),
                dbc.Col(html.Div("10"),style={"background-color": "black", "color": "white","text-align": "center"}),
                dbc.Col(html.Div("11"),style={"background-color": "coral", "color": "white","text-align": "center"}),
                dbc.Col(html.Div("12"),style={"background-color": "black", "color": "white","text-align": "center"}),
            ]
            )
    return row_ruler

def create_compare_stock_box():
    compare_stock_bar = dbc.Row(
        [
            dbc.Col(dbc.Button("Del", color="danger",block=True, id="compare_stock_del_button"), width =1, style={"text-align": "center"}),
            dbc.Col(dbc.Input(type="text", placeholder="Stock Code",id="compare_stock_input_text", valid=False,invalid=False), width = 2, style={"text-align": "center",'padding-left':'5px','padding-right':'5px'}),
            dbc.Col(dbc.Button("Add", color="success",block=True, id="compare_stock_add_button"), width =1, style={"text-align": "center"}),
            dbc.Col(dbc.Button("Clear", color="warning",block=True, id="compare_stock_clear_button"), width =1, style={"text-align": "center",'padding-left':'5px'}),
        ],
        no_gutters=True,
        className="ml-auto flex-nowrap",
        align="center",
        style = {"padding-bottom": "10px"}
    )

    compare_stock_loading_logo =  dbc.Row(
        [
            dbc.Col(dcc.Loading(id="compare_stock_loading_logo",
        type="dot",
        #type="circle",
        children=html.A([dbc.Button("Compare", color="primary",block=True, id="compare_stock_start_button"),html.A('',id="compare_stock_loading_logo_output")]))
            , width = 5)
        ],
        no_gutters=True,
        className="ml-auto flex-nowrap",
        align="center",
        style = {"padding-bottom": "10px"}
    )
    compare_stock_box = html.A(
        [compare_stock_bar,compare_stock_loading_logo],
        style = {"padding-bottom": "15px"}
    )

    return compare_stock_box


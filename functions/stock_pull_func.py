import pandas as pd
from bs4 import BeautifulSoup
import urllib.request as ur
import yfinance as yf
import datetime
import re
import datefinder
import numpy as np

def check_for_date(input_string):
    try:
        matches = list(datefinder.find_dates(str(input_string)))
        if len(matches) > 0:
            return True
        else:
            return False
    except:
        return False

def read_in_yahoo_statistics(tgt_website):
    # function code taken from : https://simply-python.com/2019/01/16/retrieving-stock-statistics-from-yahoo-finance-using-python/
    # The web page is make up of several html table. By calling read_html function.
    # all the tables are retrieved in dataframe format.
    # Next is to append all the table and transpose it to give a nice one row data.
    df_list = pd.read_html(tgt_website)
    result_df = df_list[0]
 
    for df in df_list[1:]:
        result_df = result_df.append(df)
 
    # The data is in column format.
    # Transpose the result to make all data in single row

    result_df = result_df.rename(columns={0:'variable',1:'value'})
    result_df['variable'] = result_df['variable'].apply(lambda x: x[:-1].strip() if x[-1].isdigit() else x.strip())
    result_df = result_df.reset_index(drop=True)
    result_df = result_df.set_index('variable')
    return result_df

def read_in_yahoo_financials(input_url):
    read_data = ur.urlopen(input_url).read() 
    soup_is= BeautifulSoup(read_data,'lxml')
    ls= [] # Create empty list
    for l in soup_is.find_all('div'):
        #Find all data structure that is 'div’
        ls.append(l.string) # add each element one by one to the list

    ls = [e for e in ls if e not in ('Operating Expenses','Non-recurring Events')] 

    new_ls = list(filter(None,ls))
    #new_ls = new_ls[12:]
    new_ls_idx = [True if (check_for_date(i) or i in ['ttm']) else False for i in new_ls ] 
    new_ls = new_ls[(np.where(new_ls_idx)[0][1]-1):]
    search_flag = [pd.notnull(re.search('[a-zA-Z]', i)) for i in new_ls]
    number_of_columns = search_flag.index(True,2)
    is_data = list(zip(*[iter(new_ls)]*number_of_columns))
    df = pd.DataFrame(is_data[0:])
    if df.iloc[0,0]=='Quarterly':
        df.iloc[0,0] = 'date'

    df.columns = list(df.iloc[0]) # Name columns to first row of dataframe
    df = df.iloc[1:,] # start to read 1st row
    df = df.T # transpose dataframe
    df.columns = list(df.iloc[0]) #Name columns to first row of dataframe
    df.drop(df.index[0],inplace=True) #Drop first index row
    df = df.loc[df.index!='ttm',:]
    return df

def read_in_yahoo_data(stock_symbol,period = "max"):
    stock_info = dict()
    stock_object = yf.Ticker(stock_symbol)
    # pull from yahoo fiance using yf.Ticker for sttock price
    stock_info['stock_symbol'] = stock_symbol
    try:
        stock_info.update(stock_object.info)
    except Exception as e:
        print('missing stock info. Stock info cannot be extracted.' +'\n'+ str(e))
    try:
        tempdata = stock_object.history(period=period).reset_index()
        tempdata.columns = [x.lower() for x in tempdata.columns]
        stock_info['history'] =  tempdata
    except Exception as e:
        print('missing stock history. Stock info cannot be extracted.' +'\n'+ str(e))
        stock_info['history'] = None

    # pull from yahoo fiance for financial data
    # URL link 
    url_is = r'https://uk.finance.yahoo.com/quote/' + stock_symbol + '/financials?p=' + stock_symbol + "&.tsrc=fin-srch"
    url_bs = r'https://uk.finance.yahoo.com/quote/' + stock_symbol + '/balance-sheet?p=' + stock_symbol + "&.tsrc=fin-srch"
    url_cf = r'https://uk.finance.yahoo.com/quote/' + stock_symbol + '/cash-flow?p=' + stock_symbol + "&.tsrc=fin-srch"
    url_ks = r'https://uk.finance.yahoo.com/quote/' + stock_symbol + '/key-statistics?p=' + stock_symbol + "&.tsrc=fin-srch"

    try:
        stock_info['income_statement'] = read_in_yahoo_financials(url_is)
    except:
        stock_info['income_statement'] = None
    try:
        stock_info['balance_sheet'] = read_in_yahoo_financials(url_bs)
    except:
        stock_info['balance_sheet'] = None
    try:
        stock_info['cash_flow'] = read_in_yahoo_financials(url_cf)
    except:
        stock_info['cash_flow'] = None
    try:
        stock_info['key_statistics'] = read_in_yahoo_statistics(url_ks)
    except:
        stock_info['key_statistics'] = None

    return stock_info

def check_for_valid_code(stock_symbol):
    stock_object = yf.Ticker(stock_symbol)
    try:
        df_temp = stock_object.history(period="1d")
        if df_temp.shape[0]<=0:
            return False
        else:
            return True
    except Exception as e:
        print('missing stock history. Stock info cannot be extracted.' +'\n'+ str(e))
        return False
        
if __name__ == "__main__":
    stock_symbol = r"D05.SI"
    print("Valid stock_symbol: {}".format(check_for_valid_code(stock_symbol)))
    stock_info = read_in_yahoo_data(stock_symbol)
    print(stock_info.keys())

    print(stock_info['income_statement'])
    print(stock_info['income_statement'].columns)

    print(stock_info['key_statistics'])
    print(stock_info['key_statistics'].columns)
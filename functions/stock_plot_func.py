import pandas as pd
import datetime
import plotly.graph_objects as go
from plotly.subplots import make_subplots

def plot_stock_price(stock_price_df, plot_name):
    # this function will plot the stock price and dividend amount in the same plot

    stock_price = stock_price_df.copy()

    # Create figure with secondary y-axis
    fig = make_subplots(specs=[[{"secondary_y": True}]])

    # Create traces for price plot
    trace_1 = go.Scatter(
        x = stock_price['date'], 
        y= stock_price['close'],
        mode='lines',
        name='price',
        line = dict(color='royalblue', width=3)
        )

    # Create traces for dividend plot
    trace_2 = go.Bar(
        x= stock_price.loc[stock_price['dividends']>0,'date'],
        y= stock_price.loc[stock_price['dividends']>0,'dividends'],
        marker_color='rgb(216,31,42)',
        marker_line_color='rgb(216,31,42)',
        marker_line_width=0.1,
        opacity=0.6,
        name='dividends',
        )

    fig.add_trace(trace_1, secondary_y=False)
    fig.add_trace(trace_2, secondary_y=True)

    fig.update_yaxes(title_text='price ($)', ticks = "inside", tickformat = '.3f', secondary_y=False)
    fig.update_yaxes(title_text='dividend amount ($)', ticks = "inside", tickformat = '.3f', secondary_y=True)
    fig.update_xaxes(title_text='date', ticks = "inside", tickformat='%Y-%m-%d')

    fig.update_layout(
        title = {'text': plot_name},
        legend_orientation="h",
        legend=dict(x=0, y=-.5),
        xaxis=dict(
            rangeselector=dict(
                buttons=list([
                    dict(count=1, label="1m", step="month", stepmode="backward"),
                    dict(count=3, label="3m", step="month", stepmode="backward"),
                    dict(count=6, label="6m", step="month", stepmode="backward"),
                    dict(count=1, label="YTD", step="year", stepmode="todate"),
                    dict(count=1, label="1y", step="year", stepmode="backward"),
                    dict(count=2, label="2y", step="year", stepmode="backward"),
                    dict(count=3, label="3y", step="year", stepmode="backward"),
                    dict(count=5, label="5y", step="year", stepmode="backward"),
                    dict(step="all")
                ])
            ),
            rangeslider=dict(
                visible=True
            ),
            type="date"
        )
    )

    return fig

def plot_table(table_df, plot_name, height=30):
    # this function will plot tables
    df = table_df.copy()
    column_list = [i.replace("'","") for i in list(df.columns)]
    fig = go.Figure(data=[go.Table( 
        columnwidth = [200,50],
        header=dict(values=[i.title() for i in column_list],
        height=height,
        align=['center'],
        line_color='darkslategray'),
        cells=dict(values=[df[i] for i in column_list],
        height=height,
        align=['center'],
        line_color='darkslategray')),
    ])
    fig.update_layout(
        title = {'text': plot_name})
    return fig